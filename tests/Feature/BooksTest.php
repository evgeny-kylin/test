<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class BooksTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexTest()
    {
        $response = $this->get('/api/books');
        $response->assertStatus(200);
        $response->assertJsonCount(15, 'data');

    }

    public function testSearchTest()
    {
        $title = Book::inRandomOrder()->first()->title;
        $response = $this->get('/api/books?s='.$title);
        $response->assertStatus(200);
        $response->assertJson(['data' => [0 => ['title' => $title]]]);
    }

    public function testShowTest()
    {
        $book = Book::inRandomOrder()->first();
        $response = $this->get('/api/books/'.$book->id);
        $response->assertStatus(200);
        $response->assertJson(['title' => $book->title]);

    }

    public function testCreateTest()
    {
        Book::where(['title' => 'title'])->orWhere(['title' => 'title2'])->delete();
        $user = User::inRandomOrder()->first();
        $response = $this->post('/api/books', [
            'title' => 'title',
            'description' => 'description',
            'year' => 2020,
            'user_ids' => [$user->id]
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'title' => 'title',
            'description' => 'description',
            'year' => 2020,
            'users' => [$user->fio]
        ]);
    }

    public function testUpdateTest()
    {
        $book = Book::where(['title' => 'title'])->first();
        $user = User::inRandomOrder()->first();
        $response = $this->json('PUT', '/api/books/'.$book->id, [
            'title' => 'title2',
            'description' => 'description2',
            'year' => 2019,
            'user_ids' => [$user->id]
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'title' => 'title2',
            'description' => 'description2',
            'year' => 2019,
            'users' => [$user->fio]
        ]);
    }

    public function testDeleteTest()
    {
        $book = Book::where(['title' => 'title2'])->first();
        $response = $this->json('DELETE', '/api/books/'.$book->id);
        $response->assertStatus(200);
        $response->assertJson(['success' => 1]);
        $this->assertTrue(Book::where(['title' => 'title2'])->count() === 0);
    }

}
