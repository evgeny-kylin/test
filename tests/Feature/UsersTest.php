<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UsersTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexTest()
    {
        $response = $this->get('/api/users');
        $response->assertStatus(200);
        $this->assertTrue(count($response->json('data')) > 4);
    }

    public function testSearchTest()
    {
        $last_name = User::inRandomOrder()->first()->last_name;
        $response = $this->get('/api/users?s='.$last_name);
        $response->assertStatus(200);
        $response->assertJson(['data' => [0 => ['last_name' => $last_name]]]);
    }

    public function testShowTest()
    {
        $user = User::inRandomOrder()->first();
        $response = $this->get('/api/users/'.$user->id);
        $response->assertStatus(200);
        $response->assertJson(['last_name' => $user->last_name]);
    }

    public function testCreateTest()
    {
        User::where(['last_name' => 'last_name'])->orWhere(['last_name' => 'last_name2'])->delete();
        $response = $this->post('/api/users', [
            'email' => 'test@test.test',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'middle_name' => 'middle_name',
            'birth_year' => 1900,
            'dead_year' => 2000,
        ]);
        $response->assertStatus(200);
        $response->assertJson([
            'email' => 'test@test.test',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'middle_name' => 'middle_name',
            'birth_year' => 1900,
            'dead_year' => 2000,
            'books_count' => 0
        ]);
    }

    public function testUpdateTest()
    {
        $user = User::where(['last_name' => 'last_name'])->first();

        $response = $this->json('PUT', '/api/users/'.$user->id, [
            'email' => 'test1@test.test',
            'first_name' => 'first_name1',
            'last_name' => 'last_name1',
            'middle_name' => 'middle_name1',
            'birth_year' => 1901,
            'dead_year' => 2001,
        ]);

        $response->assertStatus(200);

        $response->assertJson([
            'email' => 'test1@test.test',
            'first_name' => 'first_name1',
            'last_name' => 'last_name1',
            'middle_name' => 'middle_name1',
            'birth_year' => 1901,
            'dead_year' => 2001,
        ]);
    }

    public function testDeleteTest()
    {
        $user = User::where(['last_name' => 'last_name1'])->first();
        $response = $this->json('DELETE', '/api/users/'.$user->id);
        $response->assertStatus(200);
        $response->assertJson(['success' => 1]);
        $this->assertTrue(User::where(['last_name' => 'last_name2'])->count() === 0);
    }

}
