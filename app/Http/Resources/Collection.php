<?php

namespace App\Http\Resources;

class Collection
{

    public static function pagination($pagination, $class)
    {
        $classResource = '\App\Http\Resources\\'.$class.'Resource';
        return [
            'data' => $classResource::collection($pagination->getCollection()),
            'current_page' => $pagination->currentPage(),
            'per_page' => (int)$pagination->perPage(),
            'pages' => $pagination->lastPage(),
            'total' => $pagination->total(),
        ];
    }
}
