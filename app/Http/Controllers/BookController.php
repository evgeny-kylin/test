<?php

namespace App\Http\Controllers;

use App\Http\Resources\BookCollection;
use App\Http\Resources\BookResource;
use App\Http\Resources\Collection;
use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {
        $request->validate([
            's' => 'nullable|string'
        ]);

        $query = Book::query()->with('users');

        if($request->get('s')){
            $query = $query->where('title', 'like', '%'.$request->get('s').'%');
        }

        return response()->json(Collection::pagination($query->paginate(), 'Book'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->validate([
            'title' => 'required|string|min:1|max:191',
            'description' => 'nullable|string|max:1000',
            'year' => 'integer|max:'.date('Y'),
            'user_ids' => 'nullable|array',
            'user_ids.*' => 'integer|exists:users,id'
        ]);
        $book = Book::create($requestData);
        if($request->get('user_ids')){
            $book->users()->sync($request->get('user_ids'));
        }
        $book->load('users');
        return response()->json(new BookResource($book));
    }

    /**
     * Display the specified resource.
     *
     * @param  Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        $book->load('users');
        return response()->json(new BookResource($book));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $requestData = $request->validate([
            'title' => 'required|string|min:1|max:191',
            'description' => 'nullable|string|max:1000',
            'year' => 'integer|max:'.date('Y'),
            'user_ids' => 'nullable|array',
            'user_ids.*' => 'integer|exists:users,id'
        ]);

        $book->update($requestData);
        if($request->get('user_ids')){
            $book->users()->sync($request->get('user_ids'));
        }
        $book->load('users');
        return response()->json(new BookResource($book));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book  $book)
    {
        $book->delete();
        return response()->json(['success' => true]);
    }
}
