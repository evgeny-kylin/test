<?php

namespace App\Http\Controllers;

use App\Http\Resources\Collection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {
        $request->validate([
            's' => 'nullable|string'
        ]);

        $query = User::query()->withCount('books');

        if($request->get('s')){
            $query = $query->where('last_name', 'like', '%'.$request->get('s').'%');
        }

        return response()->json(Collection::pagination($query->paginate(), 'User'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->validate([
            'email' => 'required|email|unique:users,email',
            'first_name' => 'required|string|min:1|max:191',
            'last_name' => 'required|string|min:1|max:191',
            'middle_name' => 'nullable|string|min:1|max:191',
            'birth_year' => 'nullable|integer|min:1|max:'.date('Y'),
            'dead_year' => 'nullable|integer|min:1|max:'.date('Y'),
        ]);
        $requestData['password'] = bcrypt('secret');
        $user = User::create($requestData);
        $user->loadCount('books');
        return response()->json(new UserResource($user));
    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User  $user)
    {
        $user->loadCount('books');
        return response()->json(new UserResource($user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User  $user)
    {
        $requestData = $request->validate([
            'email' => 'required|email|unique:users,email,'.$user->id,
            'first_name' => 'required|string|min:1|max:191',
            'last_name' => 'required|string|min:1|max:191',
            'middle_name' => 'nullable|string|min:1|max:191',
            'birth_year' => 'nullable|integer|min:1|max:'.date('Y'),
            'dead_year' => 'nullable|integer|min:1|max:'.date('Y'),
        ]);
        $user->update($requestData);
        $user->loadCount('books');
        return response()->json(new UserResource($user));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User  $user)
    {
        $user->delete();
        return response()->json(['success' => true]);
    }
}
