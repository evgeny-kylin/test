<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(5)->create();
        Book::factory(20)->create();

        Book::get()->map(function ($item){
            $users = User::inRandomOrder()->limit(rand(1, 3))->get();
            $item->users()->sync($users->pluck('id')->toArray());
        });
    }
}
